#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    project_settings = 'project.settings.%s' % os.getenv("ENVIRONMENT")
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", project_settings)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
