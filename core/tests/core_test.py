from django.test import TestCase
from django.test.client import Client


class CoreTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index_view_must_return_http_200(self):
        response = self.client.get('/')
        expected = 200
        self.assertEqual(response.status_code, expected)
