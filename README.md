# Django Quickstart #

Projeto básico para outros projetos Django que eu começar.

### Montando ambiente ###

    $ python bootstrap

### Ative o Virtualenv! ###

    $ source bin/activate

### Executando testes ###

    $ python manage.py test

ou

    $ make test

### Executando a aplicação (Desenvolvimento) ###

    $ python manage.py runserver --settings=project.settings.development

ou 

    $ make run

### Dependências ###

#### Python:
    * Django 1.5
    * Unipath

#### JavaScript:
    * Twitter Bootstrap
    * JQuery